import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import ListingsScreen from "../screens/ListingsScreen";
import ListingDetailsScreen from "../screens/ListingDetailsScreen";
import ListingEditScreen from "../screens/ListingEditScreen";
import MapViewScreen from "../screens/MapViewScreen";

const Stack = createStackNavigator();

const DeliveryNavigator = () => (
    <Stack.Navigator mode="modal" screenOptions={{ headerShown: false }}>
        <Stack.Screen name="ListingEdit" component={ListingEditScreen} />
        <Stack.Screen name="MapView" component={MapViewScreen} />
    </Stack.Navigator>
);

export default DeliveryNavigator;
