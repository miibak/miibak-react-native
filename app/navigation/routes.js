export default Object.freeze({
  LISTING_DETAILS: "ListingDetails",
  LISTING_EDIT: "ListingEdit",
  MAKE_DELIVERY: "MakeDelivery",
  MAP_VIEW: "MapView",
  LOGIN: "Login",
  MESSAGES: "Messages",
  REGISTER: "Register",
});
