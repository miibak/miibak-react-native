import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import AccountNavigator from "./AccountNavigator";
import FeedNavigator from "./FeedNavigator";
import ListingEditScreen from "../screens/ListingEditScreen";
import NewListingButton from "./NewListingButton";
import routes from "./routes";
import navigation from "./rootNavigation";
import useNotifications from "../hooks/useNotifications";
import MakeDeliveryScreen from "../screens/MakeDeliveryScreen";

const Tab = createBottomTabNavigator();

const AppNavigator = () => {
  // useNotifications();

  return (
    <Tab.Navigator>
      {/*<Tab.Screen*/}
      {/*  name="Feed"*/}
      {/*  component={FeedNavigator}*/}
      {/*  options={{*/}
      {/*    tabBarIcon: ({ color, size }) => (*/}
      {/*      <MaterialCommunityIcons name="home" color={color} size={size} />*/}
      {/*    ),*/}
      {/*  }}*/}
      {/*/>*/}
      <Tab.Screen
        name={routes.MAKE_DELIVERY}
        component={MakeDeliveryScreen}
        options={({ navigation }) => ({
          tabBarButton: () => (
            <NewListingButton
              onPress={() => navigation.navigate(routes.MAKE_DELIVERY)}
            />
          ),
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="plus-circle"
              color={color}
              size={size}
            />
          ),
        })}
      />
      <Tab.Screen
        name="Account"
        component={AccountNavigator}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default AppNavigator;
