import React, {useState} from "react";
import {ImageBackground, StyleSheet, View, Image, Text, Modal, Dimensions} from "react-native";

import Button from "../components/Button";
import routes from "../navigation/routes";
import MapView, {PROVIDER_GOOGLE} from "react-native-maps";
import {MaterialIcons} from "@expo/vector-icons";
import Screen from "../components/Screen";
import AppTextInput from "../components/TextInput";
import IonicontInput from "../components/IoniconInput";
import MaterialIconInput from "../components/MaterialIconInput";

function LocationSearchScreen({ navigation }) {

    const [location, setLocation] = useState({
        latitude: 51.5079145,
        longitude: -0.0899163
    });
    const [region, setRegion] = useState({
        latitude: 51.5079145,
        longitude: -0.0899163,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
    });

    return (

        <Screen style={styles.containerMap}>
            <View style={styles.searchBox}>

                <MaterialIconInput
                    icon="my-location"
                    placeholder="Point de départ"
                />

                <MaterialIconInput
                    icon="location-on"
                    placeholder="Destination"
                />


            </View>
            <MapView
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={styles.mapStyle}
                region={ region}

            >



                <MapView.Marker
                    draggable
                    title="Maintenez pour déplacer"
                    coordinate={location}
                    onDragEnd={(e) => {setLocation( {...e.nativeEvent.coordinate,latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421})
                    }}
                >
                    {/*<View style={{alignItems:"center"}}>*/}
                    {/*    <MaterialIcons name="place"  size={32} color={"#e5250a"} />*/}
                    {/*    <Text style={styles.textColorRed}>Maintenez pour déplacer</Text>*/}
                    {/*</View>*/}
                </MapView.Marker>
            </MapView>
        </Screen>
    );
}

const styles = StyleSheet.create({
    containerMap: {
        flex: 1,

    },
    mapStyle: {
        flex:2,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },

    searchBox: {
        flex:0.5,
        backgroundColor: '#fff',
        width: '100%',
        alignSelf:'center',
        borderRadius: 5,
        padding: 10,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 10,
    }
});

export default LocationSearchScreen;
