import React, { useState } from "react";
import { StyleSheet } from "react-native";
import * as Yup from "yup";

import {
  Form,
  FormField,
  FormPicker as Picker,
  SubmitButton,
} from "../components/forms";
import CategoryPickerItem from "../components/CategoryPickerItem";
import Screen from "../components/Screen";
import FormImagePicker from "../components/forms/FormImagePicker";
import listingsApi from "../api/listings";
import useLocation from "../hooks/useLocation";
import UploadScreen from "./UploadScreen";
import {Button} from "react-native";
import LocationPicker from "../components/LocationPicker";

const validationSchema = Yup.object().shape({
  startingPoint: Yup.string().required().min(1).label("Point de départ"),
  destination: Yup.string().required().min(1).max(10000).label("Price"),
  description: Yup.string().label("Description"),
  category: Yup.object().required().nullable().label("Category"),
  images: Yup.array().min(1, "Please select at least one image."),
});

const categories = [
  {
    backgroundColor: "#fc5c65",
    icon: "motorbike",
    label: "Colis Moto",
    value: 1,
  },
  {
    backgroundColor: "#fd9644",
    icon: "car",
    label: "Colis Voiture",
    value: 2,
  },
  {
    backgroundColor: "#fed330",
    icon: "truck",
    label: "Colis Camion",
    value: 3,
  },

];

function ListingEditScreen({navigation}) {
  const [uploadVisible, setUploadVisible] = useState(false);
  const [progress, setProgress] = useState(0);
  const  [location, setLocation]= useState(useLocation());


  const handleSubmit = async (listing, { resetForm }) => {
    setProgress(0);
    setUploadVisible(true);
    const result = await listingsApi.addListing(
        { ...listing, location },
        (progress) => setProgress(progress)
    );

    if (!result.ok) {
      setUploadVisible(false);
      return alert("Could not save the listing");
    }

    resetForm();
  };

  return (
      <Screen style={styles.container}>
        <UploadScreen
            onDone={() => setUploadVisible(false)}
            progress={progress}
            visible={uploadVisible}
        />
        <Form
            initialValues={{
              startingPoint: "",
              price: "",
              description: "",
              category: null,
              images: [],
            }}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
        >
          <FormImagePicker name="images" />
          <LocationPicker
              location={location}
              name="startingPoint"
              selectedItem={location}
              onSelectItem={item=>{setLocation(item);
              }}
              placeholder="Point de départ"/>



          <FormField
              name="destination"
              placeholder="Destination"

          />
          <Picker
              items={categories}
              name="category"
              numberOfColumns={3}
              PickerItemComponent={CategoryPickerItem}
              placeholder="Category"

          />
          <FormField
              maxLength={255}
              multiline
              name="description"
              numberOfLines={3}
              placeholder="Description"
          />
          <SubmitButton title="Post" />
        </Form>
      </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});
export default ListingEditScreen;
