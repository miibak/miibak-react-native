import React, { useState } from "react";
import { StyleSheet,Alert } from "react-native";
import * as Yup from "yup";

import Screen from "../components/Screen";
import usersApi from "../api/users";
import stripeApi from "../api/stripe";
import authApi from "../api/auth";
import useAuth from "../auth/useAuth";
import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import useApi from "../hooks/useApi";
import ActivityIndicator from "../components/ActivityIndicator";


const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
const usernameMustBeginWithLetter=/^[a-zA-Z].*$/
const usernameMustEndWithLetterOrNumber=/([a-zA-Z0-9])$/
const usernameRegex=/^[a-zA-Z0-9]+([_ - .]?[a-zA-Z0-9]){1}$/

const validationSchema = Yup.object().shape({
  email: Yup
      .string()
      .required("Quel est votre adresse e-mail ?")
      .email("Votre mot de passe n'est pas valide")
      ,
  password: Yup
      .string()
      .required("Renseigner un mot de passe")
      .min(8,"Votre mot de passe doit contenir au moins 8 caractères")
      ,
  phone:Yup.
  string()
      .required("Quel est votre numéro de téléphone ?")
      .min(8,"Votre numéro doit contenir 8 chiffres")
      .matches(phoneRegExp, 'Votre numéro est invalide')
      ,
  lastname:Yup
      .string()
      .required("Quel est votre nom ?"),
  firstname:Yup
      .string()
      .required("Quel est votre prénom ?"),
  username:Yup
      .string()
      .required("Indiquer un nom d'utilisateur")
      .matches(usernameMustBeginWithLetter, "Votre nom d'utilisateur doit commencer par une lettre")
      .min(3,"Le nom d'utilisateur doit contenir au moins 3 caractères")
      .matches(usernameRegex,
          "Votre nom d'utilisateur doit seulement contenir des lettres latines, nombres et un de \" _ \",\" - \",\" . \" mais pas de caractères spéciaux")
      .matches(usernameMustEndWithLetterOrNumber, "Votre nom d'utilisateur doit finir par une lettre ou un nombre")


});

function RegisterScreen({navigation}) {
  const registerApi = useApi(usersApi.register);
  const testExistEmailApi = useApi(usersApi.testExistEmail);
  const testExistUserNameApi = useApi(usersApi.testExistUserName);
  const createCustomerApi = useApi(stripeApi.createCustomer);
  const loginApi = useApi(authApi.login);
  const auth = useAuth();
  const [error, setError] = useState();

  const handleSubmit = async (userInfo) => {
    const resUsername = await testExistUserNameApi.request(userInfo);
    const resEmail = await testExistEmailApi.request(userInfo);

    if(!resUsername.ok){
      if (resUsername.data) setError(resUsername.data.error);
      else{
        setError("Une erreur inattendue s'est produite, veuillez réessayer");
        console.log(resUsername);
      }
      return;
    }
    else{
      if (resUsername.data.isSuccess){
        setError("Le nom d'utilisateur indiqué est déjà utilisé");
        return;
      }
    }

    if(!resEmail.ok){
      if (resEmail.data) setError(resEmail.data.error);
      else{
        setError("Une erreur inattendue s'est produite, veuillez réessayer");
        console.log(resEmail);
      }
      return;
    }
    else{
      if (resEmail.data.isSuccess){
        setError("Un compte est associé à l'adresse e-mail indiqué");
        return;
      }
    }



    const resultCreateCustomer = await createCustomerApi.request(userInfo);

    if (!resultCreateCustomer.ok) {
      if (resultCreateCustomer.data) setError(resultCreateCustomer.data.error);
      else {
        setError("Une erreur inattendue s'est produite, veuillez réessayer");
        console.log(resultCreateCustomer);
      }
    }
    else{
      var userWithCustomerId={
        ...userInfo,
        customerId:resultCreateCustomer.data.customer.id
      }
      const result = await registerApi.request(userWithCustomerId);

      if (!result.ok) {
        if (result.data) setError(result.data.error);
        else {
          setError("Une erreur inattendue s'est produite, veuillez réessayer");
          console.log(result);
        }
      }
      else{
        if(result.data.isSuccess){
          Alert.alert(
              'Bienvenue sur MiiBak',
              'Confirmez votre adresse électronique puis connectez vous',
              [


                {text: 'OK',onPress: ()=>{
                    navigation.navigate('Login');
                  }}
              ],
              {cancelable: false}
          );
        }
        else{
          Alert.alert(
              'Oops',
              "Une erreur inattendue s'est produite, veuillez réessayer",
              [

                {
                  text: 'Réessayer'
                }
              ],
              {cancelable: true}
          );
        }
      }


    }







  };

  return (
      <>
        <ActivityIndicator visible={registerApi.loading || loginApi.loading} />
        <Screen style={styles.container}>
          <Form
              initialValues={{ firstname:'',lastname:'', username:'',  phone:'',email: '', password: ''}}
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
          >
            <ErrorMessage error={error} visible={error} />
            <FormField
                autoCorrect={false}
                icon="account"
                name="firstname"
                placeholder="Prénom"
            />
            <FormField
                autoCorrect={false}
                icon="account"
                name="lastname"
                placeholder="Nom"
            />
            <FormField
                autoCorrect={false}
                icon="account"
                autoCapitalize="none"
                name="username"
                placeholder="Nom d'utilisateur"
            />
            <FormField
                autoCorrect={false}
                icon="phone"
                name="phone"
                placeholder="Numéro de téléphone"
                keyboardType='numeric'
                maxLength={8}
            />
            <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="email"
                keyboardType="email-address"
                name="email"
                placeholder="Email"
                textContentType="emailAddress"
            />
            <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="lock"
                name="password"
                placeholder="Password"
                secureTextEntry
                textContentType="password"
            />
            <SubmitButton title="Register" />
          </Form>
        </Screen>
      </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});

export default RegisterScreen;
