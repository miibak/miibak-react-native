import React, { useState } from "react";
import { StyleSheet, Image } from "react-native";
import * as Yup from "yup";

import Screen from "../components/Screen";
import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import authApi from "../api/auth";
import useAuth from "../auth/useAuth";
import ActivityIndicator from "../components/ActivityIndicator";
import useApi from "../hooks/useApi";

const validationSchema = Yup.object().shape({
  emailOrUsername: Yup.string().required().label("Email ou nom d'utilisateur"),
  password: Yup.string().required().min(4).label("Mot de passe"),
});

function LoginScreen(props) {
  const auth = useAuth();
  const loginApi = useApi(authApi.login);

  const [loginFailed, setLoginFailed] = useState(false);

  const handleSubmit = async (userInfo) => {
    const result = await loginApi.request(userInfo);
    if (!result.ok || !result.data.isSuccess) {
      return setLoginFailed(true);
    }

    setLoginFailed(false);
    auth.logIn(result.data.token);
  };

  return (
      <>
      <ActivityIndicator visible={loginApi.loading} />

  <Screen style={styles.container}>
      <Image style={styles.logo} source={require("../assets/logo-red.png")} />

      <Form
        initialValues={{ emailOrUsername: "", password: "" }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        <ErrorMessage
          error="Identifiant et/ou mot de passe incorrect"
          visible={loginFailed}
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          icon="email"
          keyboardType="email-address"
          name="emailOrUsername"
          placeholder="Email ou nom d'utilisateur"
          textContentType="emailAddress"
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          icon="lock"
          name="password"
          placeholder="Mot de passe"
          secureTextEntry
          textContentType="password"
        />
        <SubmitButton title="Login" />
      </Form>
    </Screen>
        </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  logo: {
    width: 80,
    height: 80,
    alignSelf: "center",
    marginTop: 50,
    marginBottom: 20,
  },
});

export default LoginScreen;
