import React, { useState } from "react";
import { StyleSheet } from "react-native";
import * as Yup from "yup";

import {
    Form,
    FormField,
    FormPicker as Picker,
    SubmitButton,
} from "../components/forms";
import CategoryPickerItem from "../components/CategoryPickerItem";
import Screen from "../components/Screen";
import UploadScreen from "./UploadScreen";
import LocationPicker from "../components/LocationPicker";
import deliveryApi from "../api/delivery";

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/


const validationSchema = Yup.object().shape({
    startingPoint: Yup.object().required("Indiquer le point de départ").nullable(),
    destination: Yup.object().required("Indiquer la destination").nullable(),
    description: Yup.string().label("Description"),
    category: Yup.object().required("Choisisssez le type de votre colis").nullable(),
    receiverPhone: Yup.
    string()
        .required("Quel est le numéro de téléphone du destinataire?")
        .min(8,"Le numéro doit contenir 8 chiffres")
        .matches(phoneRegExp, 'Le numéro est invalide'),
});

const categories = [
    {
        backgroundColor: "#fc5c65",
        icon: "motorbike",
        label: "Colis Moto",
        value: 1,
    },
    {
        backgroundColor: "#fd9644",
        icon: "car",
        label: "Colis Voiture",
        value: 2,
    },
    {
        backgroundColor: "#fed330",
        icon: "truck",
        label: "Colis Camion",
        value: 3,
    },

];



function MakeDeliveryScreen({navigation}) {
    const [uploadVisible, setUploadVisible] = useState(false);
    const [progress, setProgress] = useState(0);
    const  [startingPoint, setStartingPoint]= useState();
    const  [destination, setDestination]= useState();

    const handleSubmit = async (delivery, { resetForm }) => {
        setProgress(0);
        setUploadVisible(true);
        const result = await deliveryApi.addDelivery(
            delivery ,
            (progress) => setProgress(progress)
        );

        if (!result.ok) {
            console.log(result.problem)
            setUploadVisible(false);
            return alert("Une erreur est survenue, nous n'avons pas reussi à enregistrer votre livraison");
        }
        else{
            if (!result.isSuccess){
                setUploadVisible(false);
                return alert("Une erreur est survenue, nous n'avons pas reussi à enregistrer votre livraison");
            }
        }

        resetForm();
    };

    return (
        <Screen style={styles.container}>
            <UploadScreen
                onDone={() => setUploadVisible(false)}
                progress={progress}
                visible={uploadVisible}
            />
            <Form
                initialValues={{
                    startingPoint: null,
                    destination: null,
                    description: null,
                    receiverPhone: "",
                    category: null,
                }}
                onSubmit={handleSubmit}
                validationSchema={validationSchema}
            >
                <LocationPicker
                    name="startingPoint"
                    selectedLocation={startingPoint}
                    onSelectLocation={location=>{setStartingPoint(location);
                    }}
                    placeholder="Point de départ"/>

                    <LocationPicker
                    name="destination"
                    selectedLocation={destination}
                    onSelectLocation={location=>{setDestination(location);
                    }}
                    placeholder="Destination"/>



                <FormField
                    name="receiverPhone"
                    placeholder="Numéro du destinataire"
                    keyboardType='numeric'
                    maxLength={8}

                />
                <Picker
                    items={categories}
                    name="category"
                    numberOfColumns={3}
                    PickerItemComponent={CategoryPickerItem}
                    placeholder="Categorie du colis"

                />
                <FormField
                    maxLength={255}
                    multiline
                    name="description"
                    numberOfLines={3}
                    placeholder="Description (optionnel). Indiquer les informations nécessaires à la prise en charge de votre colis"
                />
                <SubmitButton title="Post" />
            </Form>
        </Screen>
    );
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
});
export default MakeDeliveryScreen;
