import React, { useState  } from "react";
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { StyleSheet, Text, View, Dimensions,TouchableOpacity } from 'react-native';
import useLocation from "../hooks/useLocation";
import AppGooglePlacesAutoComplete from "../components/GooglePlacesInput";
import Screen from "../components/Screen";
import colors from "../config/colors";
import AppButton from "../components/Button";
import {ActivityIndicator} from "react-native";
import AppTextInput from "../components/TextInput";

function MapViewScreen({onSelectedLocation, placeholder}) {
    const currentLocation = useLocation;
    const  [location, setLocation]= useState(currentLocation);

    console.log(location);



    return (

        <Screen style={styles.container}>


            <MapView
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={styles.mapStyle}
                region={location}

            >

                <AppGooglePlacesAutoComplete
                    style={styles.search}
                    placeholder={placeholder}
                    onPress={(data, details = null) => {
                        // 'details' is provided when fetchDetails = true
                        console.log(details.geometry);
                        setLocation({latitude:details.geometry.location.lat,
                            longitude: details.geometry.location.lng,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421})
                    }}

                />

                <View style={styles.bottomView}>
                    <AppButton title="Valider" onPress={onSelectedLocation} />
                </View>




                <MapView.Marker
                    draggable
                    coordinate={location}
                    onDragEnd={(e) => {setLocation( {...e.nativeEvent.coordinate,latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421})
                    }}
                />
            </MapView>

            <View style={styles.searchBox}>
                <TextInput
                    placeholder="Search here"
                    placeholderTextColor="#000"
                    autoCapitalize="none"
                    style={{flex:1,padding:0}}
                />
                <Ionicons name="ios-search" size={20} />
            </View>
            <ScrollView
                horizontal
                scrollEventThrottle={1}
                showsHorizontalScrollIndicator={false}
                height={50}
                style={styles.chipsScrollView}
                contentInset={{ // iOS only
                    top:0,
                    left:0,
                    bottom:0,
                    right:20
                }}
                contentContainerStyle={{
                    paddingRight: Platform.OS === 'android' ? 20 : 0
                }}
            >
                {state.categories.map((category, index) => (
                    <TouchableOpacity key={index} style={styles.chipsItem}>
                        {category.icon}
                        <Text>{category.name}</Text>
                    </TouchableOpacity>
                ))}
            </ScrollView>


        </Screen>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    button: {

    },
    bottomView: {
        width: '30%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute', //Here is the trick
        bottom: 70, //Here is the trick
        right:0
    },
    search: {
        backgroundColor:"#ffffff"
    },
    text: {
        color: colors.white,
        fontSize: 18,
        textTransform: "uppercase",
        fontWeight: "bold",
    },
    mapStyle: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
    searchBox: {
        position:'absolute',
        marginTop: Platform.OS === 'ios' ? 40 : 20,
        flexDirection:"row",
        backgroundColor: '#fff',
        width: '90%',
        alignSelf:'center',
        borderRadius: 5,
        padding: 10,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 10,
    },
    scrollView: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        paddingVertical: 10,
    }
});

export default MapViewScreen;
