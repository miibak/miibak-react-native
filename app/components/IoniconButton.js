import React, { Component } from 'react';
import {StyleSheet, TextInput, View, Text, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export function IoniconButton({name, style, onPress}){

    return (
        <TouchableOpacity style={[styles.touchable,style]} onPress={onPress}>
            <Ionicons name={name}  size={32} color={"#7E57C2"} />
        </TouchableOpacity>


    )
}

const styles= StyleSheet.create({
    touchable:{},



});
