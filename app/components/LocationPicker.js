import React, { useState,useEffect } from "react";
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    Modal,
    TextInput,
    ScrollView,
    Button,
    FlatList, Dimensions, ActivityIndicator, TouchableOpacity,
} from "react-native";
import {Ionicons, MaterialCommunityIcons, MaterialIcons} from "@expo/vector-icons";

import Text from "./Text";
import Screen from "./Screen";
import defaultStyles from "../config/styles";
import PickerItem from "./PickerItem";
import MapViewScreen from "../screens/MapViewScreen";
import MapView, {PROVIDER_GOOGLE} from "react-native-maps/index";
import AppGooglePlacesAutoComplete from "./GooglePlacesInput";
import AppButton from "./Button";
import colors from "../config/colors";
import useLocation from "../hooks/useLocation";
import ErrorMessage from "./forms/ErrorMessage";
import {useFormikContext} from "formik";
import CustomMarker from "./CustomMarker";
import useRegionLocation from "../hooks/useRegionLocation";

function LocationPicker({ icon, name, onSelectLocation, placeholder, selectedLocation}) {
    const [modalVisible, setModalVisible] = useState(false);

    const [location, setLocation] = useState({
        latitude: 51.5079145,
        longitude: -0.0899163
    });
    const [region, setRegion] = useState({
        latitude: 51.5079145,
        longitude: -0.0899163,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
    });

    const { errors, setFieldValue, touched, values } = useFormikContext();

    return (
        <>
            <TouchableWithoutFeedback
                onPress={() => setModalVisible(true)}>
                <View style={styles.container}>
                    {icon && (
                        <MaterialCommunityIcons
                            name={icon}
                            size={20}
                            color={defaultStyles.colors.medium}
                            style={styles.icon}
                        />
                    )}
                    {selectedLocation ? (
                        <Text
                        value={values[name]}
                            style={styles.text}>{selectedLocation.latitude}</Text>
                    ) : (
                        <Text style={styles.placeholder}>{placeholder}</Text>
                    )}


                </View>
            </TouchableWithoutFeedback>
            <ErrorMessage error={errors[name]} visible={touched[name]} />

            <Modal visible={modalVisible} animationType="slide">

                <Screen style={styles.containerMap}>
                    <View style={styles.header}>
                        <Button style={styles.buttonHeader} title="Close" onPress={() => setModalVisible(false)} />
                        <Button style={styles.buttonHeader} title="Valider" onPress={()=>{
                            setModalVisible(false);
                            onSelectLocation(location)
                            setFieldValue(name,location)
                        }} />
                    </View>





                    <MapView
                        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                        style={styles.mapStyle}
                        region={ region}

                    >



                        <MapView.Marker
                            draggable
                            title="Maintenez pour déplacer"
                            coordinate={location}
                            onDragEnd={(e) => {setLocation( {...e.nativeEvent.coordinate,latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421})
                            }}
                        >
                            <View style={{alignItems:"center"}}>
                                <MaterialIcons name="place"  size={32} color={"#e5250a"} />
                                <Text style={styles.textColorRed}>Maintenez pour déplacer</Text>
                            </View>
                        </MapView.Marker>
                    </MapView>

                    <View style={styles.searchBox}>
                    <AppGooglePlacesAutoComplete
                        style={styles.search}
                        placeholder={placeholder}
                        onPress={(data, details = null) => {

                            setLocation({latitude:details.geometry.location.lat,
                                longitude: details.geometry.location.lng});
                            setRegion({latitude:details.geometry.location.lat,
                                longitude: details.geometry.location.lng,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421})
                        }}

                    />
                    </View>
                    {/*<View style={styles.searchBox}>*/}
                    {/*    <TextInput*/}
                    {/*        placeholder="Search here"*/}
                    {/*        placeholderTextColor="#000"*/}
                    {/*        autoCapitalize="none"*/}
                    {/*        style={{flex:1,padding:0}}*/}
                    {/*    />*/}
                    {/*    <Ionicons name="ios-search" size={20} />*/}
                    {/*</View>*/}
                    {/*<ScrollView*/}
                    {/*    horizontal*/}
                    {/*    scrollEventThrottle={1}*/}
                    {/*    showsHorizontalScrollIndicator={false}*/}
                    {/*    height={50}*/}
                    {/*    style={styles.chipsScrollView}*/}
                    {/*    contentInset={{ // iOS only*/}
                    {/*        top:0,*/}
                    {/*        left:0,*/}
                    {/*        bottom:0,*/}
                    {/*        right:20*/}
                    {/*    }}*/}
                    {/*    contentContainerStyle={{*/}
                    {/*        paddingRight: Platform.OS === 'android' ? 20 : 0*/}
                    {/*    }}*/}
                    {/*>*/}
                    {/*    /!*{state.categories.map((category, index) => (*!/*/}
                    {/*    /!*    <TouchableOpacity key={index} style={styles.chipsItem}>*!/*/}
                    {/*    /!*        {category.icon}*!/*/}
                    {/*    /!*        <Text>{category.name}</Text>*!/*/}
                    {/*    /!*    </TouchableOpacity>*!/*/}
                    {/*    /!*))}*!/*/}
                    {/*</ScrollView>*/}


                </Screen>
            </Modal>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: defaultStyles.colors.light,
        borderRadius: 25,
        flexDirection: "row",
        width: "100%",
        padding: 15,
        marginVertical: 10,
    },
    textColorRed:{
        color: "#e5250a"
    },
    icon: {
        marginRight: 10,
    },
    text: {
        flex: 1,
    },
    header:{
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    buttonHeader:{
        flex:1
    },
    containerMap: {
        flex: 1,

    },
    button: {

    },
    search: {

        position:'absolute',
        marginTop: Platform.OS === 'ios' ? 40 : 20,
    },

    mapStyle: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
    placeholder: {
        color: defaultStyles.colors.medium,
        flex: 1,
    },
    searchBox: {
        position:'absolute',
        marginTop: Platform.OS === 'ios' ? 40 : 20,
        flexDirection:"row",
        backgroundColor: '#fff',
        width: '90%',
        alignSelf:'center',
        borderRadius: 5,
        padding: 10,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 10,
    },
    scrollView: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        paddingVertical: 10,
    },
    chipsScrollView: {
        position:'absolute',
        top:Platform.OS === 'ios' ? 90 : 80,
        paddingHorizontal:10
    }
});

export default LocationPicker;
