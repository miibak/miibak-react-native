import React from "react";
import { Text  } from "react-native";

import defaultStyles from "../config/styles";
import MapView from "react-native-maps/index";
import View from "react-native";
import {MaterialCommunityIcons} from "@expo/vector-icons";

function CustomMarker({ children, name,location, style, ...otherProps }) {
    return (


        <MaterialCommunityIcons
            name={name}
            size={40}
            color={defaultStyles.colors.primary}

        >
            <Text >
                {children}
            </Text>
        </MaterialCommunityIcons>


    );
}

export default CustomMarker;
