import React from "react";
import { View, TextInput, StyleSheet } from "react-native";
import {Ionicons, MaterialIcons} from "@expo/vector-icons";

import defaultStyles from "../config/styles";

function MaterialIconInput({ icon, width = "100%", ...otherProps }) {
    return (
        <View style={[styles.container, { width }]}>
            {icon && (
                <MaterialIcons
                    name={icon}
                    size={20}
                    color={defaultStyles.colors.medium}
                    style={styles.icon}
                />
            )}
            <TextInput
                placeholderTextColor={defaultStyles.colors.medium}
                style={defaultStyles.text}
                {...otherProps}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: defaultStyles.colors.light,
        borderRadius: 25,
        flexDirection: "row",
        padding: 5,
        marginVertical: 10,
        alignItems: 'center'
    },
    icon: {
        marginRight: 10,

    },
});

export default MaterialIconInput;
