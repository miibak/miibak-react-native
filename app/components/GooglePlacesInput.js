import React from "react";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

function AppGooglePlacesAutoComplete({ onPress, ...otherProps }) {
    return (


    <GooglePlacesAutocomplete
        onPress={onPress}
        query={{
            key: "AIzaSyBI32jJKEnP-Eg6jniLEU0C3hxmos19__Q",
            language: 'fr',
        }}
        autoFocus={true}
        fetchDetails = {true}
        // currentLocation={true}
        // currentLocationLabel='Position actuelle'
        {...otherProps}
    />
    );
}

export default AppGooglePlacesAutoComplete;
