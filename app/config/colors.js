export default {
  primary: "#FF8F00",
  secondary: "#7FFFD4",
  black: "#000",
  white: "#fff",
  medium: "#6e6969",
  light: "#f8f4f4",
  dark: "#0c0c0c",
  danger: "#ff5252",
};
