import client from "./client";

const register = (userInfo) => client.post("/users/registration", userInfo);
const testExistEmail = (userInfo) => client.post("/users/testExistEmail", userInfo);
const testExistUserName = (userInfo) => client.post("/users/testExistUserName", userInfo);

export default { register, testExistEmail, testExistUserName };
