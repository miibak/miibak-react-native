import client from "./client";

const createCustomer = (userInfo) => client.post("/createCustomer", userInfo);

export default { createCustomer };
