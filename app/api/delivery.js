import client from "./client";

const endpoint = "/delivery";
const endpointMakeDelivery = "/makeDelivery";

const getDelivery = () => client.get(endpoint);

export const addDelivery = (delivery, onUploadProgress) => {
    delivery.category=delivery.category.value;
    return client.post(endpointMakeDelivery, delivery, {
        onUploadProgress: (progress) =>
            onUploadProgress(progress.loaded / progress.total),
    });
};

export default {
    addDelivery,
    getDelivery,
};
