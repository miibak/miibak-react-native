import React, { useState,useEffect  } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { AppLoading } from "expo";

import navigationTheme from "./app/navigation/navigationTheme";
import AppNavigator from "./app/navigation/AppNavigator";
import OfflineNotice from "./app/components/OfflineNotice";
import AuthNavigator from "./app/navigation/AuthNavigator";
import AuthContext from "./app/auth/context";
import authStorage from "./app/auth/storage";
import { navigationRef } from "./app/navigation/rootNavigation";
import LoginScreen from "./app/screens/LoginScreen";
import AccountScreen from "./app/screens/AccountScreen";
import MapViewScreen from "./app/screens/MapViewScreen";
import LottieView from "lottie-react-native";
import {View} from "react-native";
import styles from "./app/config/styles";
import LocationSearchScreen from "./app/screens/LocationSearchScreen";

export default function App() {
    const [user, setUser] = useState();
    const [isReady, setIsReady] = useState(false);

    const restoreUser = async () => {
        const user = await authStorage.getUser();
        if (user) setUser(user.data);
        setIsReady(true)
    };

    useEffect(()=>{
        setTimeout( async ()=>{

            await restoreUser();
            setIsReady(true)
        },3500)
    },[])

    if (!isReady)
        return (
            // <AppLoading startAsync={restoreUser} onFinish={() => setIsReady(true)} />

            <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
                <LottieView
                    autoPlay
                    loop={true}
                    onAnimationFinish={restoreUser}
                    source={require("./app/assets/animations/delivery.json")}
                />

            </View>

        );

    return (
        // <AuthContext.Provider value={{ user, setUser }}>
        //     <OfflineNotice />
        //     <NavigationContainer ref={navigationRef} theme={navigationTheme}>
        //         {user ? <AppNavigator /> : <AuthNavigator />}
        //     </NavigationContainer>
        // </AuthContext.Provider>

        <LocationSearchScreen/>
    );




}
